package com.parmaia.gameserver.httpserver;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.parmaia.gameserver.exception.DuplicateControllerException;
import com.parmaia.gameserver.model.BaseStorage;
import com.parmaia.gameserver.utils.Log;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Routes the requests to the corresponding registered controller. Also handles
 * errors not handled by controllers
 * 
 * @author parmaia
 *
 */
public class ControllerRouter implements HttpHandler {
  private HashMap<String, BaseHttpController> controllerList;
  private BaseStorage storage;

  public ControllerRouter(BaseStorage storage) {
    controllerList = new LinkedHashMap<String, BaseHttpController>();
    this.storage = storage;
  }

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    String uri = exchange.getRequestURI().toString();

    String[] tokens = uri.split("[/?]");
    int status = 400;// Bad request
    // Find the controller
    boolean handled = false;
    if (controllerList.containsKey(tokens[2])) {
      try {
        handled = controllerList.get(tokens[2]).handleRequest(exchange, tokens, storage);
      } catch (Exception e) {
        Log.error(e.getMessage());
        exchange.sendResponseHeaders(500, 0); // Internal Server Error
        exchange.getResponseBody().close();
        return;
      }
    } else {
      Log.error("Method not implemented: " + tokens[2]);
      status = 404;// Not found
    }
    if (!handled) {
      exchange.sendResponseHeaders(status, 0);
      exchange.getResponseBody().close();
    }
  }

  public void registerController(BaseHttpController controller) throws DuplicateControllerException {
    if (controllerList.containsKey(controller.getMethodName()))
      throw new DuplicateControllerException("Controller for " + controller.getMethodName() + " has already registered");
    else
      controllerList.put(controller.getMethodName(), controller);
  }

}
