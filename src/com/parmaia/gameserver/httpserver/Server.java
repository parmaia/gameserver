package com.parmaia.gameserver.httpserver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import com.parmaia.gameserver.exception.DuplicateControllerException;
import com.parmaia.gameserver.exception.StorageNotImplementedException;
import com.parmaia.gameserver.model.StorageFactory;
import com.parmaia.gameserver.model.StorageType;
import com.parmaia.gameserver.utils.Log;
import com.sun.net.httpserver.HttpServer;

/**
 * Server for handling all requests, and communication with clients. 
 * @author parmaia
 *
 */
public class Server {
  private HttpServer server;
  private ControllerRouter router;

  public Server(int port, StorageType storageType) throws IOException, StorageNotImplementedException {
    Log.message("Creating server at port " + port + " ...");
    server = HttpServer.create(new InetSocketAddress(port), 0);

    Log.message("Setting handler...");
    router = new ControllerRouter(StorageFactory.createStorage(storageType));
    server.createContext("/", router);

    Log.message("Setting Thread Pool model...");
    server.setExecutor(Executors.newCachedThreadPool());
  }

  public void start() {
    // Start the server
    Log.message("Starting server... (stop it by killing the process)");
    server.start();
  }

  public void registerController(BaseHttpController controller) throws DuplicateControllerException {
    router.registerController(controller);
  }

  public void stop() {
    server.stop(0);
  }
}
