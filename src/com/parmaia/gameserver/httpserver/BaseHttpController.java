package com.parmaia.gameserver.httpserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import com.parmaia.gameserver.model.BaseStorage;
import com.parmaia.gameserver.utils.Log;
import com.parmaia.gameserver.utils.StringUtils;
import com.sun.net.httpserver.HttpExchange;

/**
 * Base class for all controllers. Specifies the basic operations that a controller should do, and
 * provides convenient methods for retrieving parameters and sending the response back to the client.
 * @author parmaia
 *
 */

public abstract class BaseHttpController {
  public abstract boolean handleRequest(HttpExchange exchange, String[] params, BaseStorage storage) throws Exception;

  public abstract String getMethodName();

  protected void sendHttpResponse(HttpExchange exchange, int statusCode, String response) throws Exception {
    if (response != null) {
      exchange.sendResponseHeaders(statusCode, response.length());
      OutputStream os = exchange.getResponseBody();
      os.write(response.getBytes());
      os.close();
    } else {
      exchange.sendResponseHeaders(statusCode, 0);
      exchange.getResponseBody().close();
    }
  }

  protected void parseGetParams(String queryString,
      Map<String, String> parameters) {
    try {
      StringUtils.parseQueryString(queryString, parameters);
    } catch (UnsupportedEncodingException e) {
      Log.error(e.getMessage());
    }
  }

  protected void parsePostParams(InputStream requestBody,
      Map<String, String> parameters) {
    InputStreamReader isr;
    try {
      isr = new InputStreamReader(requestBody, "utf-8");
      BufferedReader br = new BufferedReader(isr);
      String query = br.readLine();
      StringUtils.parseQueryString(query, parameters);
    } catch (IOException e) {
      Log.error(e.getMessage());
    }
  }

}
