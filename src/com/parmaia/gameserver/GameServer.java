package com.parmaia.gameserver;

import java.io.IOException;

import com.parmaia.gameserver.controller.HighScoreListController;
import com.parmaia.gameserver.controller.LoginController;
import com.parmaia.gameserver.controller.ScoreController;
import com.parmaia.gameserver.exception.DuplicateControllerException;
import com.parmaia.gameserver.exception.StorageNotImplementedException;
import com.parmaia.gameserver.httpserver.Server;
import com.parmaia.gameserver.model.StorageType;
import com.parmaia.gameserver.utils.Log;

public class GameServer {
  /**
   * Main entry for the server.
   * 
   * @param args The first parameter is the optional port number.
   * @throws IOException 
   * @throws DuplicateControllerException 
   * @throws StorageNotImplementedException 
   */
  public static void main(String[] args) throws IOException, DuplicateControllerException, StorageNotImplementedException {
    int defaultPort = 8081;
    int port = defaultPort;
    if(args.length >= 1){
      try{
        port = Integer.parseInt(args[0]);
      }catch(NumberFormatException e){
        Log.error(e.getMessage());
      }
    }
    
    //First create a server instance
    Server server = new Server(port, StorageType.MEMORY);
    
    //Then register as many controllers as needed
    server.registerController(new LoginController());
    server.registerController(new ScoreController());
    server.registerController(new HighScoreListController());
    
    //Finally start the server
    server.start();
  }

}
