package com.parmaia.gameserver.exception;

public class DuplicateControllerException extends Exception {
  private static final long serialVersionUID = 1L;

  public DuplicateControllerException(String msg) {
    super(msg);
  }
}
