package com.parmaia.gameserver.exception;

public class StorageNotImplementedException extends Exception {
  private static final long serialVersionUID = 1L;

  public StorageNotImplementedException(String msg) {
    super(msg);
  }
}
