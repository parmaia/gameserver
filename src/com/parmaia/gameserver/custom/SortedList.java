package com.parmaia.gameserver.custom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * List that keeps objects ordered according to a custom sortComparator. It
 * keeps the list in order and limited to a number of elements.
 * 
 * If a uniqueComparator is provided, also the list is kept with unique
 * elements.
 * 
 * Also, provides a handy <code>join(String sep)</code> method that returns a
 * string with all the elements of the list separated by <code>sep</code> if the
 * class <code>T</code> has a nice <code>toString()</code> implementation.
 * 
 * This list is Thread-Safe.
 * 
 * @author parmaia
 *
 * @param <T>
 */
public class SortedList<T> {
  private ArrayList<T> list;
  private final Comparator<T> sortComparator;
  private final Comparator<T> uniqueComparator;
  private final int limit;

  /**
   * Constructor for the list.
   * 
   * Using this constructor (with no uniqueComparator) allow the list to have
   * duplicates
   * 
   * @param sortComparator
   *          Custom comparator used to sort objects of type <code>T</code>
   */
  public SortedList(Comparator<T> sortComparator) {
    this(sortComparator, null, -1);
  }

  /**
   * Constructor for the list.
   * 
   * Using this constructor (with uniqueComparator) prevents duplicated items on
   * the list.
   * 
   * @param sortComparator
   *          Custom comparator used to sort objects of type <code>T</code>
   * @param uniqueComparator
   *          Custom comparator used to determine uniqueness of objects of type
   *          <code>T</code>
   */
  public SortedList(Comparator<T> sortComparator, Comparator<T> uniqueComparator) {
    this(sortComparator, uniqueComparator, -1);
  }

  /**
   * Constructor for the list.
   * 
   * Using this constructor (with uniqueComparator) prevents duplicated items on
   * the list, and keep the list to a maximum of <code>limit</code> elements.
   * 
   * @param sortComparator
   *          Custom comparator used to sort objects of type <code>T</code>
   * @param uniqueComparator
   *          Custom comparator used to determine uniqueness of objects of type
   *          <code>T</code>
   * @param limit
   *          max number of elements to keep on the list
   */
  public SortedList(Comparator<T> sortComparator,
      Comparator<T> uniqueComparator, int limit) {
    list = new ArrayList<T>();
    this.sortComparator = sortComparator;
    this.uniqueComparator = uniqueComparator;
    this.limit = limit;
  }

  /**
   * This insertion keeps the list sorted and also, if there is a
   * uniqueComparator, the items are unique according to that comparator.
   * 
   * @param object
   */
  public synchronized void insert(T object) {
    int i;
    if (uniqueComparator != null) {
      // First search for a duplicate entry
      for (i = 0; i < list.size(); i++) {
        if (uniqueComparator.compare(list.get(i), object) == 0) {
          // the object exists, just remove the previous occurrence if it is
          // lower than the previous.
          if (sortComparator.compare(list.get(i), object) > 0)
            list.remove(i);
          else
            // If the previous occurrence is greater, don't do anything
            return;
          break;
        }
      }
    }
    // If a limit is set, and list size got this limit
    if (limit > 0 && list.size() >= limit) {
      if (sortComparator.compare(list.get(list.size() - 1), object) < 0)
        // The object that is to be inserted, is less than the last element of the list (the lesser one), so
        // this object wont be added
        return;
      else
        // Remove the last item on the list to make room for the new one.
        list.remove(list.size() - 1);
    }
    i = Collections.binarySearch(list, object, sortComparator);
    if (i < 0) //binarySearch returns the closest position where the object should be, but in a negative index. 
      i = -(i + 1);
    list.add(i, object);
  }

  /**
   * Show a string representation of the list.
   */
  public String toString() {
    String str = "";
    synchronized (list) {
      for (T item : list) {
        str += "[" + item.toString() + "] ";
      }
    }
    return str;
  }

  /**
   * Join list elements using space as separator
   * @return
   */
  public String join() {
    return join(" ");
  }

  /**
   * Join list elements using <code>separator</code>
   * @param separator
   * @return
   */
  public String join(String separator) {
    String str = "";
    synchronized (list) {
      for (T item : list) {
        str += item.toString() + separator;
      }
    }
    if (str.length() > 0)
      str = str.substring(0, str.length() - separator.length());
    return str;
  }

  public synchronized T get(int index) {
    return list.get(index);
  }

  public synchronized int size() {
    return list.size();
  }

  public synchronized void clear() {
    list.clear();
  }
}
