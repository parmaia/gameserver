package com.parmaia.gameserver.model;

/**
 * Enum for easy selection of the storage type
 * 
 * @author parmaia
 *
 */
public enum StorageType {
  MEMORY, DISK, DATABASE, NETWORK;
}
