package com.parmaia.gameserver.model;

/**
 * Interface that specifies all the operations that a storage implementation
 * should have
 * 
 * @author parmaia
 *
 */
public interface BaseStorage {
  public int getUserId(String sessionKey);
  public void putSessionKey(int userId, String sessionKey);
  public void putScore(int level, int userId, int score);
  public String getScoreList(int level);
}
