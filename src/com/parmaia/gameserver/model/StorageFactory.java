package com.parmaia.gameserver.model;

import com.parmaia.gameserver.exception.StorageNotImplementedException;

/**
 * Storage factory for creating the various types of Storages. The seleccion can
 * be done based on various criterias
 * 
 * @author parmaia
 *
 */

public class StorageFactory {
  public static BaseStorage createStorage(StorageType storageType) throws StorageNotImplementedException {
    // Selection can be done based on a config file, environment, etc...
    switch (storageType) {
    case MEMORY:
      return new MemoryStorage();
    case DATABASE:
    case DISK:
    case NETWORK:
      throw new StorageNotImplementedException("Storage type " + storageType + " not implemented yet");
    }
    return null;
  }
}
