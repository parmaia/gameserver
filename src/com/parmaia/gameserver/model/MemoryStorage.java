package com.parmaia.gameserver.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.parmaia.gameserver.custom.SortedList;
import com.parmaia.gameserver.entity.Score;
import com.parmaia.gameserver.entity.Session;
import com.parmaia.gameserver.utils.Constants;

/**
 * In memory data storage
 * 
 * @author parmaia
 *
 */
public class MemoryStorage implements BaseStorage {
  private Map<String, Session> sessions;
  private Map<Integer, SortedList<Score>> scores;

  public MemoryStorage() {
    sessions = new ConcurrentHashMap<String, Session>();
    scores = new ConcurrentHashMap<Integer, SortedList<Score>>();
  }

  @Override
  public int getUserId(String sessionId) {
    Session s = sessions.get(sessionId);
    boolean expired;
    if (s == null)
      return -1;
    expired = s.hasExpired();
    if (!expired)
      return s.getUserId();
    else {
      sessions.remove(sessionId);
      return -1;
    }
  }

  @Override
  public void putSessionKey(int userId, String sessionKey) {
    Session s = new Session(userId, sessionKey);
    sessions.put(sessionKey, s);
  }

  @Override
  public void putScore(int level, int userId, int score) {
    if (scores.get(level) == null) {
      synchronized (scores) {
        if (scores.get(level) == null) { // Double check locking
          scores.put(level, new SortedList<Score>(Score.sortComparator, Score.uniqueComparator, Constants.LIMIT_SCORES));
        }
      }
    }
    scores.get(level).insert(new Score(userId, score));
  }

  @Override
  public String getScoreList(int level) {
    SortedList<Score> list = scores.get(level);
    if (list == null)
      return "";
    else
      return list.join(",");
  }

}
