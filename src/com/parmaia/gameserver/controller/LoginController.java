package com.parmaia.gameserver.controller;

import com.parmaia.gameserver.httpserver.BaseHttpController;
import com.parmaia.gameserver.model.BaseStorage;
import com.parmaia.gameserver.utils.Log;
import com.parmaia.gameserver.utils.StringUtils;
import com.sun.net.httpserver.HttpExchange;

/**
 * Controller for the login
 * 
 * @author parmaia
 *
 */
public class LoginController extends BaseHttpController {

  @Override
  public boolean handleRequest(HttpExchange exchange, String[] params, BaseStorage storage) throws Exception {
    int userId = Integer.parseInt(params[1]);
    Log.message("Login request for user: " + userId);
    String sessionKey = getSessionKey();
    storage.putSessionKey(userId, sessionKey);
    sendHttpResponse(exchange, 200, sessionKey);
    return true;
  }

  private String getSessionKey() {
    return StringUtils.randomString(10);
  }

  @Override
  public String getMethodName() {
    return "login";
  }
}
