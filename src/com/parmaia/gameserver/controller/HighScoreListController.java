package com.parmaia.gameserver.controller;

import com.parmaia.gameserver.httpserver.BaseHttpController;
import com.parmaia.gameserver.model.BaseStorage;
import com.parmaia.gameserver.utils.Log;
import com.sun.net.httpserver.HttpExchange;

/**
 * Controller for the Highscore list.
 * 
 * @author parmaia
 *
 */
public class HighScoreListController extends BaseHttpController {

  @Override
  public boolean handleRequest(HttpExchange exchange, String[] params, BaseStorage storage) throws Exception {
    int level = Integer.parseInt(params[1]);
    Log.message("Highscore list request for level: " + level);
    String response = storage.getScoreList(level);
    sendHttpResponse(exchange, 200, response);
    return true;
  }

  @Override
  public String getMethodName() {
    return "highscorelist";
  }

}
