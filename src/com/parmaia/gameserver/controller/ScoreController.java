package com.parmaia.gameserver.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import com.parmaia.gameserver.httpserver.BaseHttpController;
import com.parmaia.gameserver.model.BaseStorage;
import com.parmaia.gameserver.utils.Log;
import com.sun.net.httpserver.HttpExchange;

/**
 * Controller for posting score
 * 
 * @author parmaia
 *
 */
public class ScoreController extends BaseHttpController {
  private static final String PARAMETER_SCORE = "score";
  private static final String PARAMETER_SESSIONKEY = "sessionkey";

  @Override
  public boolean handleRequest(HttpExchange exchange, String[] params, BaseStorage storage) throws Exception {
    // Only post requests
    if (!"post".equalsIgnoreCase(exchange.getRequestMethod()))
      return false;

    int level = Integer.parseInt(params[1]);
    Log.message("Post score request for level: " + params[1]);
    Map<String, String> parameters = new LinkedHashMap<String, String>();

    parseGetParams(params[3], parameters);
    parsePostParams(exchange.getRequestBody(), parameters);

    String sessionKey = parameters.get(PARAMETER_SESSIONKEY);
    if (sessionKey == null)
      return false;
    // if a NumberFormatException is thrown, the DefaultHandler will handle it
    // and generate an error 500 response.
    int score = Integer.parseInt(parameters.get(PARAMETER_SCORE));

    int userId = storage.getUserId(sessionKey);
    if (userId == -1) {
      sendHttpResponse(exchange, 403, "");// Forbidden
      Log.message("sessionKey not found or expired");
      return true;
    } else {
      storage.putScore(level, userId, score);
      sendHttpResponse(exchange, 200, "");// Ok
      return true;
    }
  }

  @Override
  public String getMethodName() {
    return "score";
  }

}
