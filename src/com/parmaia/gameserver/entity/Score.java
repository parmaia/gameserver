package com.parmaia.gameserver.entity;

import java.util.Comparator;

/**
 * User score pair. Implements methods for comparing two objects for sorting and
 * for uniquenes.
 * 
 * @author parmaia
 *
 */
public class Score {
  private int userId;
  private int score;

  public Score(int userId, int score) {
    this.userId = userId;
    this.score = score;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public String toString() {
    return userId + "=" + score;
  }

  /*
   * these are the comparators used to keep the score list ordered by score and
   * unique by userId
   */
  public static Comparator<Score> sortComparator = new Comparator<Score>() {
    @Override
    public int compare(Score o1, Score o2) {
      int cmp = o2.getScore() - o1.getScore();
      return cmp != 0 ? cmp : -1; // this keeps the oldest user-score first in the list.
    }
  };

  public static Comparator<Score> uniqueComparator = new Comparator<Score>() {
    @Override
    public int compare(Score o1, Score o2) {
      return o2.getUserId() - o1.getUserId();
    }
  };
}
