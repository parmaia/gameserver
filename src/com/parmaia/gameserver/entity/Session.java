package com.parmaia.gameserver.entity;

import com.parmaia.gameserver.utils.Constants;

/**
 * Keep track of user sessions, and it expiration.
 * 
 * @author parmaia
 *
 */
public class Session {
  private int userId;
  private String sessionKey;
  private long timeCreated;

  public Session(int userId, String sessionKey) {
    this.userId = userId;
    this.sessionKey = sessionKey;
    timeCreated = System.currentTimeMillis();
  }

  public int getUserId() {
    return userId;
  }

  public String getSessionKey() {
    return sessionKey;
  }

  public long getTimeCreated() {
    return timeCreated;
  }

  public boolean hasExpired() {
    return timeCreated + Constants.SESSION_EXPIRE_MILLIS < System.currentTimeMillis();
  }
}
