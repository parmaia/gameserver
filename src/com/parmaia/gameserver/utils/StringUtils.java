package com.parmaia.gameserver.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class for handling various common string operations
 * 
 * @author parmaia
 *
 */
public class StringUtils {
  // Allowed characters in the sessionKey
  private static final String CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  public static String randomString(int len) {
    StringBuilder sb = new StringBuilder(len);
    for (int i = 0; i < len; i++)
      sb.append(CHARS.charAt(ThreadLocalRandom.current().nextInt(CHARS.length())));
    return sb.toString();
  }

  public static void parseQueryString(String queryString, Map<String, String> parameters) throws UnsupportedEncodingException {
    if (queryString != null && queryString.length() > 0) {
      String[] pairs = queryString.split("&");
      for (String pair : pairs) {
        int idx = pair.indexOf("=");
        String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
        String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
        parameters.put(key, value);
      }
    }
  }
}
