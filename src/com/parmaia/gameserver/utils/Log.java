package com.parmaia.gameserver.utils;



/**
 * Simple log class. This only shows messages to the console
 * 
 * @author parmaia
 *
 */
public class Log {
  private static boolean enabled = true;
  
  /**
   * This method is not thread-safe. Should be called in the main thread

   * @param enabled
   */
  public static void setEnabled(boolean enabled){
    Log.enabled = enabled;
  }
  
  public static void message(String message){
    log("Debug", message);
  }
  
  public static void error(String error){
    log("Error", error);
  }
  
  private static void log(String type, String txt){
    if(enabled){
      System.out.println(type+": "+ txt+" ["+threadId()+"]");
    }
  }
  
  private static long threadId(){
    return Thread.currentThread().getId();
  }
  
}
