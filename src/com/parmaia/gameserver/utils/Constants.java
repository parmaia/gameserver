package com.parmaia.gameserver.utils;

/**
 * Definition of generic constants used in the project
 * 
 * @author parmaia
 *
 */
public class Constants {
  // General constants
  public static final int MIN_TO_SEC = 60;
  public static final int SEC_TO_MILLI = 1000;

  // Session expire time
  public static final int SESSION_EXPIRE_MILLIS = 10 * MIN_TO_SEC * SEC_TO_MILLI;
  // Max number of scores to keep
  public static final int LIMIT_SCORES = 15;
}
