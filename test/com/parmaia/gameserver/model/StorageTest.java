package com.parmaia.gameserver.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.CyclicBarrier;

import org.junit.Before;
import org.junit.Test;

import com.parmaia.gameserver.custom.SortedList;
import com.parmaia.gameserver.entity.Score;
import com.parmaia.gameserver.utils.Constants;
import com.parmaia.gameserver.utils.StringUtils;

/**
 * Tests for the Storages
 * 
 * Test the storage operations: 
 *   putSesionKey() 
 *   getUserId() 
 *   putScore()
 *   getScoreList()
 * 
 * under normal and concurrent operation
 * 
 * @author parmaia
 *
 */

public class StorageTest {
  public BaseStorage storage;

  @Before
  public void setUp() throws Exception {
    // Creates the storage to test. In this case, the Memory type.
    storage = StorageFactory.createStorage(StorageType.MEMORY);
  }

  /**
   * Test storing and retrieving sessionKeys and userIds
   */
  @Test
  public void testGetUserId() {
    storage.putSessionKey(4, "ABCDEF");
    storage.putSessionKey(9, "GHIJKL");
    storage.putSessionKey(84, "MNOPQR");
    int id = storage.getUserId("GHIJKL");
    assertTrue("Incorrect userId: " + id, id == 9);
  }

  /**
   * Test storing sessionKeys and userIds but retrieving an invalid sessionKey
   */
  @Test
  public void testGetUserIdAbsent() {
    storage.putSessionKey(4, "ABCDEF");
    storage.putSessionKey(9, "GHIJKL");
    storage.putSessionKey(84, "MNOPQR");
    int id = storage.getUserId("UVWXYZ");
    assertTrue("Incorrect userId: " + id, id == -1);
  }

  @Test
  public void testPutSessionKey() {
    // PutSessionKey is tested in the testGetUserId* methods
    assertTrue(true);
  }

  /**
   * Thread to simulate concurrent putSessionKey operations.
   * 
   * The thread waits until the variable "wait" is true, so all the threads try
   * to execute the putSessionKey operation at the "same" time.
   * 
   * @author parmaia
   *
   */
  private class PutSessionKeyThread extends Thread {
    String sessionKey;
    int userId;
    CyclicBarrier gate;

    public PutSessionKeyThread(int userId, String sessionKey, CyclicBarrier gate) {
      super();
      this.userId = userId;
      this.sessionKey = sessionKey;
      this.gate = gate;
    }

    @Override
    public void run() {
      try {
        gate.await();// To make all threads run the next line simultaneously
      } catch (Exception e) {
      }
      storage.putSessionKey(userId, sessionKey);
    }
  };

  /**
   * Test storing sessionKeys and userIds in a concurrent environment.
   */
  @Test
  public void testPutSessionKeyConcurrent() {
    // How many threads to spawn
    int threadCount = 1000;
    // Gate to let the threads do its job almost at the same time.
    CyclicBarrier gate = new CyclicBarrier(threadCount + 1);
    // Random generated sessionKeys
    String[] sessionKeys = new String[threadCount];
    // Control of each one of the threads
    PutSessionKeyThread[] threads = new PutSessionKeyThread[threadCount];

    // First generate all the sessionKeys and start all the threads.
    for (int i = 0; i < threadCount; i++) {
      sessionKeys[i] = StringUtils.randomString(10);
      threads[i] = new PutSessionKeyThread(i, sessionKeys[i], gate);
      threads[i].start();
    }
    // Allow each thread to continue at the same time.
    try {
      gate.await();
    } catch (Exception e1) {
    }

    // Wait for each thread to excecute
    for (int i = 0; i < threadCount; i++) {
      try {
        threads[i].join();
      } catch (InterruptedException e) {
      }
    }
    
    // Finally retrieve each userId and test if corresponds to the correct user.
    for (int i = 0; i < threadCount; i++) {
      int id = storage.getUserId(sessionKeys[i]);
      assertTrue("Incorrect userId: " + id + " expected: " + i, id == i);
    }
  }

  /**
   * Test that getScoreList that should be ordered, and with unique userIds
   */
  @Test
  public void testGetScoreList() {
    storage.putSessionKey(4, "ABCDEF");
    storage.putSessionKey(9, "GHIJKL");
    storage.putSessionKey(84, "MNOPQR");

    storage.putScore(1, 4, 90);
    storage.putScore(1, 9, 100);
    storage.putScore(1, 84, 80);
    storage.putScore(2, 4, 120);
    storage.putScore(1, 9, 101);
    storage.putScore(1, 4, 88);

    String list = storage.getScoreList(1);
    assertTrue("Invalid score list: " + list, "9=101,4=90,84=80".equals(list));
  }

  /**
   * Test that getScoreList return an empty string if the requested level has no
   * scores.
   */
  @Test
  public void testGetScoreListAbsent() {
    storage.putSessionKey(4, "ABCDEF");
    storage.putSessionKey(9, "GHIJKL");
    storage.putSessionKey(84, "MNOPQR");

    storage.putScore(1, 4, 90);
    storage.putScore(1, 9, 100);
    storage.putScore(1, 84, 80);
    storage.putScore(2, 4, 120);
    storage.putScore(1, 9, 101);
    storage.putScore(1, 4, 88);

    String list = storage.getScoreList(3); // There is no score for level 3
    assertTrue("Invalid score list: " + list, "".equals(list));
  }

  /**
   * Test that when two or more users has the same score, the first that put his
   * score is the first that is on the list, no matter the userId ordering
   */
  @Test
  public void testGetScoreListOrder() {
    storage.putSessionKey(4, "ABCDEF");
    storage.putSessionKey(9, "GHIJKL");
    storage.putSessionKey(84, "MNOPQR");

    storage.putScore(1, 4, 100);
    storage.putScore(1, 9, 100);
    storage.putScore(1, 84, 100);
    storage.putScore(2, 4, 88);
    storage.putScore(2, 84, 88);
    storage.putScore(2, 9, 88);

    String list;
    list = storage.getScoreList(1);
    assertThat("Invalid score list", list, is("4=100,9=100,84=100"));

    list = storage.getScoreList(2);
    assertThat("Invalid score list", list, is("4=88,84=88,9=88"));
  }

  @Test
  public void testPutScore() {
    // putScore is tested in the testGetScoreList* methods
    assertTrue(true);
  }

  /**
   * Thread to simulate concurrent putScore operations.
   * 
   * The thread waits until the variable "wait" is true, so all the threads try
   * to execute the putScore operation at the "same" time.
   * 
   * @author parmaia
   *
   */
  private class PutScoreThread extends Thread {
    int userId;
    int level;
    int score;
    CyclicBarrier gate;

    public PutScoreThread(int userId, int level, int score, CyclicBarrier gate) {
      super();
      this.userId = userId;
      this.level = level;
      this.score = score;
      this.gate = gate;
    }

    @Override
    public void run() {
      try {
        gate.await(); // To make all threads run next line simultaneously
      } catch (Exception e) {
      }
      storage.putScore(level, userId, score);
    }
  };

  /**
   * Test storing scores to a level in a concurrent environment.
   * 
   * Note that this test can fail in the uneven case when two or more users has
   * the same score. The order is not guaranteed because it is not possible
   * to know in the simulation of the expected result, which thread puts its
   * score first. To avoid this, the score is generated randomly in the range
   * [0, 200000) so the chances of getting two scores equals are less probable.
   * But the order on which users post its scores is preserved and that is
   * tested in testGetScoreListOrder().
   * 
   */
  @Test
  public void testPutScoreListConcurrent() {
    // How many threads to spawn
    int threadCount = 1000;
    // Gate to let the threads do its job almost at the same time.
    CyclicBarrier gate = new CyclicBarrier(threadCount + 1);
    // List of scores to calculate the expected value.
    List<Score> scores = new Vector<Score>();
    // Control of each one of the threads
    PutScoreThread[] threads = new PutScoreThread[threadCount];
    // Initialize the random generator with a fixed seed.
    Random rnd = new Random(1234);

    // First populate the scores and start the threads
    for (int i = 0; i < threadCount; i++) {
      // Simulate at most 20 users
      scores.add(new Score(rnd.nextInt(20), rnd.nextInt(200000)));
      threads[i] = new PutScoreThread(scores.get(i).getUserId(), 1, scores.get(i).getScore(), gate);
      threads[i].start();
    }
    // Allow each thread to continue at the same time.
    try {
      gate.await();
    } catch (Exception e) {
    }
    // Wait for each thread to excecute
    for (int i = 0; i < threadCount; i++) {
      try {
        threads[i].join();
      } catch (InterruptedException e) {
      }
    }
    // Now get the score list from the concurrently accessed storage
    String csvListConcurrent = storage.getScoreList(1);

    // Finally to get the expected list, use a SortedList in a non-concurrent way.
    SortedList<Score> list = new SortedList<Score>(Score.sortComparator, 
                                                   Score.uniqueComparator, 
                                                   Constants.LIMIT_SCORES);
    for (int i = 0; i < threadCount; i++) {
      list.insert(scores.get(i));
    }

    // And this is the expected score list
    String csvListNonConcurrent = list.join(",");

    assertThat("Incorrect concurrent score list", csvListConcurrent, is(csvListNonConcurrent));

    // Also test the length of the list, it should be less or equal than the
    // limit set at creation time.
    String[] tokens = csvListConcurrent.split(",");
    assertTrue("Incorrect score list length: " + tokens.length, tokens.length <= Constants.LIMIT_SCORES);
  }
  
}
