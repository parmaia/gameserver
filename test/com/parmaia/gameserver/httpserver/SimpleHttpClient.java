package com.parmaia.gameserver.httpserver;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Simple HttpClient for send GET and POST requests
 * 
 * @author parmaia
 *
 */
public class SimpleHttpClient {
  private final String USER_AGENT = "Test/1.0";

  public class Response {
    private int responseCode;
    private String contents;

    public Response(int responseCode, String contents) {
      super();
      this.responseCode = responseCode;
      this.contents = contents;
    }

    public int getResponseCode() {
      return responseCode;
    }

    public String getContents() {
      return contents;
    }

  };

  /**
   * 
   * Http Get request
   * 
   * @param url
   * @return Response
   * @throws Exception
   */
  public Response sendGet(String url) throws Exception {
    URL obj = new URL(url);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

    con.setRequestProperty("User-Agent", USER_AGENT);

    int responseCode = con.getResponseCode();

    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();
    while ((inputLine = in.readLine()) != null) {
      response.append(inputLine);
    }
    in.close();

    return new Response(responseCode, response.toString());
  }

  /**
   * Http POST request
   * 
   * @param url
   * @param postParams
   * @return Response
   * @throws Exception
   */
  public Response sendPost(String url, String postParams) throws Exception {
    URL obj = new URL(url);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

    // add reuqest header
    con.setRequestMethod("POST");
    con.setRequestProperty("User-Agent", USER_AGENT);

    // Send post request
    con.setDoOutput(true);
    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
    wr.writeBytes(postParams);
    wr.flush();
    wr.close();

    int responseCode = con.getResponseCode();

    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      response.append(inputLine);
    }
    in.close();

    return new Response(responseCode, response.toString());
  }
}
