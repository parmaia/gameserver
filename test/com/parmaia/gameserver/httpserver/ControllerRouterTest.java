package com.parmaia.gameserver.httpserver;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import com.parmaia.gameserver.controller.HighScoreListController;
import com.parmaia.gameserver.controller.LoginController;
import com.parmaia.gameserver.controller.ScoreController;
import com.parmaia.gameserver.model.BaseStorage;
import com.parmaia.gameserver.model.StorageFactory;
import com.parmaia.gameserver.model.StorageType;

/**
 * Test all the operations (login, putt score and get high score list) through
 * the ControllerRouter
 * 
 * @author parmaia
 *
 */
public class ControllerRouterTest {
  ControllerRouter defaultHandler;

  @Before
  public void setUp() throws Exception {
    BaseStorage storage = StorageFactory.createStorage(StorageType.MEMORY);
    defaultHandler = new ControllerRouter(storage);
    defaultHandler.registerController(new LoginController());
    defaultHandler.registerController(new ScoreController());
    defaultHandler.registerController(new HighScoreListController());

    // Login and post some data...
    loginAndPostScore(131, 2, 1220);
  }

  /* Helper methods */
  public HttpExchangeMock loginAndPostScore(int userId, int level, int score) throws URISyntaxException, IOException {
    HttpExchangeMock exchange = loginUser(userId);
    exchange = postScore(exchange.getOutput(), level, score);
    return exchange;
  }

  public HttpExchangeMock postScore(String sessionKey, int level, int score) throws URISyntaxException, IOException {
    HttpExchangeMock exchange = new HttpExchangeMock("score=" + score, "POST", "/" + level + "/score?sessionkey=" + sessionKey);
    defaultHandler.handle(exchange);
    return exchange;
  }

  public HttpExchangeMock loginUser(int userId) throws URISyntaxException, IOException {
    HttpExchangeMock exchange = new HttpExchangeMock("", "GET", "/" + userId + "/login");
    defaultHandler.handle(exchange);
    return exchange;
  }
  /* End of helper methods */
  
  @Test
  public void testHandleLogin() throws URISyntaxException, IOException {
    HttpExchangeMock exchange = loginUser(4711);

    assertThat(exchange.getResponseCode(), is(200));
    assertThat((int) exchange.getResponseLength(), not(is(0)));
    assertTrue("Invalid response: " + exchange.getOutput(), exchange.getOutput().matches("[A-Za-z0-9]{10}"));
  }

  @Test
  public void testHandlePostScore() throws URISyntaxException, IOException {
    HttpExchangeMock exchange = loginAndPostScore(4711, 2, 1500);

    assertThat(exchange.getResponseCode(), is(200));
    assertThat((int) exchange.getResponseLength(), is(0));
    assertTrue("Invalid response: " + exchange.getOutput(), "".equals(exchange.getOutput()));
  }

  @Test
  public void testHandlePostScoreInvalidSessionKey() throws URISyntaxException, IOException {
    HttpExchangeMock exchange = loginUser(4771);
    exchange = postScore("InVaLiDseSsIoNkEy", 2, 1500);

    assertThat(exchange.getResponseCode(), is(403));
    assertThat((int) exchange.getResponseLength(), is(0));
    assertTrue("Invalid response: " + exchange.getOutput(), "".equals(exchange.getOutput()));
  }

  @Test
  public void testHandleGetHighScoreList() throws URISyntaxException, IOException {
    HttpExchangeMock exchange = loginAndPostScore(4711, 2, 1500);

    exchange = new HttpExchangeMock("", "GET", "/2/highscorelist");
    defaultHandler.handle(exchange);

    assertThat(exchange.getResponseCode(), is(200));
    assertThat((int) exchange.getResponseLength(), not(is(0)));
    assertTrue("Invalid response: " + exchange.getOutput(), "4711=1500,131=1220".equals(exchange.getOutput()));
  }

  @Test
  public void testHandleGetHighScoreListEmptyLevel() throws URISyntaxException, IOException {
    HttpExchangeMock exchange = loginAndPostScore(4711, 2, 1500);

    exchange = new HttpExchangeMock("", "GET", "/3/highscorelist");
    defaultHandler.handle(exchange);

    assertThat(exchange.getResponseCode(), is(200));
    assertThat((int) exchange.getResponseLength(), is(0));
    assertTrue("Invalid response: " + exchange.getOutput(), "".equals(exchange.getOutput()));
  }

}
