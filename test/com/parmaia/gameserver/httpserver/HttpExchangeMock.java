package com.parmaia.gameserver.httpserver;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpPrincipal;

/**
 * Helper class to simulate HttpExchange
 * 
 * @author parmaia
 *
 */
public class HttpExchangeMock extends HttpExchange {
  private String requestBody;
  private String method;
  private URI uri;
  private ByteArrayOutputStream byteArrayOutput;
  private int responseCode;
  private long responseLength;

  public HttpExchangeMock(String requestBody, String method, String uri) throws URISyntaxException {
    super();
    this.requestBody = requestBody;
    this.method = method;
    this.uri = new URI(uri);
  }

  @Override
  public void close() {
  }

  @Override
  public Object getAttribute(String arg0) {
    return null;
  }

  @Override
  public HttpContext getHttpContext() {
    return null;
  }

  @Override
  public InetSocketAddress getLocalAddress() {
    return null;
  }

  @Override
  public HttpPrincipal getPrincipal() {
    return null;
  }

  @Override
  public String getProtocol() {
    return null;
  }

  @Override
  public InetSocketAddress getRemoteAddress() {
    return null;
  }

  @Override
  public InputStream getRequestBody() {
    InputStream stream = new ByteArrayInputStream(requestBody.getBytes(StandardCharsets.UTF_8));
    return stream;
  }

  @Override
  public Headers getRequestHeaders() {
    return null;
  }

  @Override
  public String getRequestMethod() {
    return method;
  }

  @Override
  public URI getRequestURI() {
    return uri;
  }

  @Override
  public OutputStream getResponseBody() {
    byteArrayOutput = new ByteArrayOutputStream();
    return byteArrayOutput;
  }

  public String getOutput() {
    try {
      byteArrayOutput.flush();
    } catch (IOException e) {
    }
    return new String(byteArrayOutput.toByteArray(), StandardCharsets.UTF_8);
  }

  @Override
  public int getResponseCode() {
    return responseCode;
  }

  public long getResponseLength() {
    return responseLength;
  }

  @Override
  public Headers getResponseHeaders() {
    return null;
  }

  @Override
  public void sendResponseHeaders(int responseCode, long length) throws IOException {
    this.responseCode = responseCode;
    this.responseLength = length;
  }

  @Override
  public void setAttribute(String arg0, Object arg1) {
  }

  @Override
  public void setStreams(InputStream arg0, OutputStream arg1) {
  }

}
