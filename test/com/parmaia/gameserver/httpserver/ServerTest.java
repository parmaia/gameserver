package com.parmaia.gameserver.httpserver;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.parmaia.gameserver.controller.HighScoreListController;
import com.parmaia.gameserver.controller.LoginController;
import com.parmaia.gameserver.controller.ScoreController;
import com.parmaia.gameserver.custom.SortedList;
import com.parmaia.gameserver.entity.Score;
import com.parmaia.gameserver.httpserver.SimpleHttpClient.Response;
import com.parmaia.gameserver.model.StorageType;
import com.parmaia.gameserver.utils.Constants;

/**
 * Test the responses of the web server via Http calls. If the port is already
 * in use, this test will fail.
 * 
 * @author parmaia
 *
 */
public class ServerTest {
  int port = 8091;
  Server server;

  private String getUrl(String params) {
    return "http://localhost:" + port + "/" + params;
  }

  @Before
  public void setUp() throws Exception {
    // Create and start a fresh server
    server = new Server(port, StorageType.MEMORY);
    server.registerController(new LoginController());
    server.registerController(new ScoreController());
    server.registerController(new HighScoreListController());

    server.start();
  }

  @After
  public void tearDown() {
    server.stop();
  }

  @Test
  public void login() throws Exception {
    SimpleHttpClient client = new SimpleHttpClient();
    Response response = client.sendGet(getUrl("3/login"));
    assertThat(response.getResponseCode(), is(200));
    assertTrue(!"".equals(response.getContents()));
  }

  @Test
  public void postScore() throws Exception {
    SimpleHttpClient client = new SimpleHttpClient();
    // Login first
    Response response = client.sendGet(getUrl("88/login"));
    assertThat(response.getResponseCode(), is(200));
    String sessionKey = response.getContents();

    // Post score
    response = client.sendPost(getUrl("1/score?sessionkey=" + sessionKey), "score=3000");
    assertThat(response.getResponseCode(), is(200));
    assertTrue("".equals(response.getContents()));
  }

  @Test
  public void getHighscoreList() throws Exception {
    SimpleHttpClient client = new SimpleHttpClient();
    Random rnd = new Random(1234);
    int users = 20;
    String[] sessionKeys = new String[users];
    SortedList<Score> scores = new SortedList<Score>(Score.sortComparator, Score.uniqueComparator, Constants.LIMIT_SCORES);
    Response response;

    for (int i = 0; i < users; i++) {
      // Login and post score for each user
      response = client.sendGet(getUrl(i + "/login"));
      assertThat(response.getResponseCode(), is(200));
      sessionKeys[i] = response.getContents();

      int score = rnd.nextInt(2000);
      scores.insert(new Score(i, score));
      response = client.sendPost(getUrl("1/score?sessionkey=" + sessionKeys[i]), "score=" + score);
      assertThat(response.getResponseCode(), is(200));
    }

    // Now request the Highscore list of an empty level
    response = client.sendGet(getUrl("2/highscorelist"));
    assertThat(response.getResponseCode(), is(200));
    assertTrue("".equals(response.getContents()));

    // Now request the Highscore list of the populated level
    response = client.sendGet(getUrl("1/highscorelist"));
    assertThat(response.getResponseCode(), is(200));
    String localList = scores.join(",");
    assertThat(response.getContents(), is(localList));
  }
}
