package com.parmaia.gameserver.entity;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Field;

import org.junit.Test;

import com.parmaia.gameserver.utils.Constants;

/**
 * Test the expiration of the user session
 * 
 * @author parmaia
 *
 */
public class SessionTest {

  @Test
  public void testExpirationOfSession() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
    Session session = new Session(1, "sEsSiOnKeY");
    //Using reflection to mock the session timeCreated field.
    Field f = Session.class.getDeclaredField("timeCreated");
    f.setAccessible(true);
    //Set timeCreated back the expiration period and 10 milliseconds to ensure that now the session is expired
    f.set(session, System.currentTimeMillis()-Constants.SESSION_EXPIRE_MILLIS-10);
    assertThat(session.hasExpired(),is(true));
  }

}
