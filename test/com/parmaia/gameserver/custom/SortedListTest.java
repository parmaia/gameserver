package com.parmaia.gameserver.custom;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for the custom collection SortedList. It tests that the order, limits
 * and uniqueness of items are preserved
 * 
 * @author parmaia
 *
 */
public class SortedListTest {
  public SortedList<Integer> list;

  @Before
  public void setUp() {
    list = new SortedList<Integer>(new Comparator<Integer>() {
      @Override
      public int compare(Integer o1, Integer o2) {
        return o2 - o1;
      }
    }, new Comparator<Integer>() {
      @Override
      public int compare(Integer o1, Integer o2) {
        return o2 - o1;
      }
    }, 4);
  }

  /**
   * Test that the limit set in the constructor is kept.
   */
  @Test
  public void testInsert() {
    list.insert(4);
    list.insert(3);
    list.insert(5);
    list.insert(2);
    list.insert(6);
    assertThat("Incorrect number of elements", list.size(), is(4));
  }

  /**
   * Test the order and uniqueness of the elements. Also test the method join()
   * to get the contents of the list
   */
  @Test
  public void testJoinString() {
    String str;
    list.insert(4);
    list.insert(3);
    list.insert(5);
    list.insert(2);
    list.insert(6);
    str = list.join(",");
    assertThat("Incorrect join of elements", str, is("6,5,4,3"));
  }

}
