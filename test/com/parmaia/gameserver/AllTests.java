package com.parmaia.gameserver;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.parmaia.gameserver.custom.SortedListTest;
import com.parmaia.gameserver.entity.SessionTest;
import com.parmaia.gameserver.httpserver.ControllerRouterTest;
import com.parmaia.gameserver.httpserver.ServerTest;
import com.parmaia.gameserver.model.StorageTest;

/**
 * Test suite for all the implemented tests
 * 
 * @author parmaia
 *
 */

@RunWith(Suite.class)
@SuiteClasses({SortedListTest.class, 
               StorageTest.class, 
               SessionTest.class, 
               ControllerRouterTest.class, 
               ServerTest.class})

public class AllTests {

}
